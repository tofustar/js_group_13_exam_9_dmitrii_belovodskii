import { Component, OnInit } from '@angular/core';
import { FormArray, FormControl, FormGroup, Validators } from '@angular/forms';
import { imageValidator } from './validate-image-url.directive';
import { CocktailService } from '../shared/cocktail.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-new-cocktail',
  templateUrl: './new-cocktail.component.html',
  styleUrls: ['./new-cocktail.component.css']
})
export class NewCocktailComponent implements OnInit {
  cocktailForm!: FormGroup;

  constructor(private cocktailService: CocktailService, private router: Router) { }

  ngOnInit(): void {
    this.cocktailForm = new FormGroup({
      name: new FormControl('', Validators.required),
      image: new FormControl('', [
        Validators.required,
        imageValidator
      ]),
      type: new FormControl('', Validators.required),
      description: new FormControl(''),
      ingredients: new FormArray([]),
      instructions: new FormControl('', Validators.required)
    });
  }

  saveCocktail() {
    this.cocktailService.addCocktail(this.cocktailForm.value).subscribe(() => {
      void this.router.navigate(['']);
    });
  }

  addIng() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    const ingGroup = new FormGroup({
      ingName: new FormControl('', Validators.required),
      amount: new FormControl('', Validators.required),
      unit: new FormControl('', Validators.required)
    })
    ingredients.push(ingGroup);
  }

  getIngControls() {
    const ingredients = <FormArray>this.cocktailForm.get('ingredients');
    return ingredients.controls;
  }

  fieldHasError(fieldName: string, errorType: string) {
    const field = this.cocktailForm.get(fieldName);
    return field && field.touched && field.errors?.[errorType];
  }

  removeIng(i: number) {
    const ing = <FormArray>this.cocktailForm.get('ingredients');
    ing.removeAt(i);
  }
}
