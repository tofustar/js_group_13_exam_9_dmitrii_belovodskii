import { AbstractControl, NG_VALIDATORS, ValidationErrors, Validator } from '@angular/forms';
import { Directive } from '@angular/core';

export const imageValidator = (control: AbstractControl): ValidationErrors | null => {

  const imageString = /^http:[/][/]/.test(control.value);
  const imageStringSecondType = /^https:[/][/]/.test(control.value);

  if (imageString || imageStringSecondType) return null;

  return {imageUrl: true};
};

@Directive({
  selector: '[appImage]',
  providers: [{
    provide: NG_VALIDATORS,
    useExisting: ValidateImageUrlDirective,
    multi: true
  }]
})

export class ValidateImageUrlDirective implements Validator {
  validate(control: AbstractControl): ValidationErrors | null {
    return imageValidator(control);
  }
}
