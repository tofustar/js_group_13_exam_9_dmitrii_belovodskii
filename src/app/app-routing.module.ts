import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { CocktailsComponent } from './cocktails/cocktails.component';

const routes: Routes = [
  {path: '', component: CocktailsComponent},
  {path: 'new-cocktail', component: NewCocktailComponent},
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
