import { Component, OnInit } from '@angular/core';
import { Cocktail } from '../shared/cocktail.model';
import { Subscription } from 'rxjs';
import { CocktailService } from '../shared/cocktail.service';

@Component({
  selector: 'app-cocktails',
  templateUrl: './cocktails.component.html',
  styleUrls: ['./cocktails.component.css']
})
export class CocktailsComponent implements OnInit {
  cocktails: Cocktail[] = [];
  cocktailsChangeSubscription!: Subscription;
  cocktailDetailsOpen = false;
  currentCocktail!: Cocktail;

  constructor(private cocktailService: CocktailService) {}

  ngOnInit(): void {
    this.cocktailsChangeSubscription = this.cocktailService.cocktailsChange.subscribe((cocktails: Cocktail[]) => {
      this.cocktails = cocktails;
    });
    this.cocktailService.fetchCocktails();
  }

  openCocktailDetails(cocktail: Cocktail) {
    this.currentCocktail = cocktail;
    this.cocktailDetailsOpen = true;
  }

  closeCocktailDetails() {
    this.cocktailDetailsOpen = false;
  }
}
