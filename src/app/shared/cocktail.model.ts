export class Cocktail {
  constructor(
    public id: string,
    public name: string,
    public image: string,
    public type: string,
    public description: string,
    public ingredients: Ingredients[],
    public instructions: string,
  ) {}
}

class Ingredients {
  constructor(
    public ingName: string,
    public amount: number,
    public unit: string,
  ) {}
}
