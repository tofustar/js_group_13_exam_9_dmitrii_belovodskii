import { Injectable } from '@angular/core';
import { Cocktail } from './cocktail.model';
import { HttpClient } from '@angular/common/http';
import { Subject } from 'rxjs';
import { map } from 'rxjs/operators';

@Injectable()

export class CocktailService {
  cocktailsChange = new Subject<Cocktail[]>();
  private cocktails: Cocktail[] = [];

  constructor(private http: HttpClient) {}

  addCocktail(cocktail: Cocktail) {
    return this.http.post('https://js-group-13-default-rtdb.firebaseio.com/cocktails.json', cocktail)
  }

  fetchCocktails() {
    return this.http.get<{[key:string]: Cocktail}>('https://js-group-13-default-rtdb.firebaseio.com/cocktails.json')
      .pipe(map(result => {
        return Object.keys(result).map(id => {
          const cocktailData = result[id];
          return new Cocktail(id, cocktailData.name, cocktailData.image, cocktailData.type,
            cocktailData.description, cocktailData.ingredients, cocktailData.instructions)
        });
      }))
      .subscribe(cocktails => {
        this.cocktails = cocktails;
        this.cocktailsChange.next(this.cocktails.slice());
      });
  }

}
