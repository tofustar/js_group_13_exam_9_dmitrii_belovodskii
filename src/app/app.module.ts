import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { ToolbarComponent } from './toolbar/toolbar.component';
import { NewCocktailComponent } from './new-cocktail/new-cocktail.component';
import { ReactiveFormsModule } from '@angular/forms';
import { ValidateImageUrlDirective } from './new-cocktail/validate-image-url.directive';
import { CocktailService } from './shared/cocktail.service';
import { HttpClientModule } from '@angular/common/http';
import { CocktailsComponent } from './cocktails/cocktails.component';
import { CocktailDetailsComponent } from './cocktails/cocktail-details/cocktail-details.component';

@NgModule({
  declarations: [
    AppComponent,
    ToolbarComponent,
    NewCocktailComponent,
    ValidateImageUrlDirective,
    CocktailsComponent,
    CocktailDetailsComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    ReactiveFormsModule,
    HttpClientModule,
  ],
  providers: [CocktailService],
  bootstrap: [AppComponent]
})
export class AppModule { }
